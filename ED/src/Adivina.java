import java.util.Random;
import java.util.Scanner;
//Version 2b(en pruebas)
// Compilar con javac -g Adivina.java
// Ejecutar con java Adivina
public class Adivina {
public static void main(String[] args) throws java.io.IOException {
Random generador = new Random();
Scanner scanner = new Scanner(System.in);
int numero_ordenador = generador.nextInt(100) + 1;
int numero_humano;
int intentos = 0;
System.out.println("Adivina el número del 1 al 100");
do {
// Lee número introducido por teclado
numero_humano = scanner.nextInt();
// Da una pista si es mayor o menor
if (numero_ordenador < numero_humano) {
System.out.println("Menos");
}
if (numero_ordenador > numero_humano) {
System.out.println("Mas");
}
// Repite mientras el humano no acierte
intentos++;
} while (numero_ordenador != numero_humano && intentos < 3);
if (numero_ordenador == numero_humano) {
System.out.println("¡¡CORRECTO!!");
} else {
System.out.println("NOOOOO, ¡HAS FRACASADO!");
}
}
}